#!/usr/bin/env bash
  
NEXT_WIN=$(bspc query -N -n next.local.floating || bspc query -N -n any.local.floating)
bspc node --focus $NEXT_WIN
