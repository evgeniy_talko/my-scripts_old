#!/usr/bin/env bash
# Author: Evgeniy Talko
# email: etalko@gmail.com
# Description: Script for getting information about bluetooth keyboard

FIRST_ATTR=$1

function get_info() {
  upower -i "$(upower -e | awk '/keyboard/ {print; exit}')" # replace /keyboard/ with a cli-argument
} 

function get_info_batt() {
  upower -i "$(upower -e | awk '/keyboard/ {print; exit}')" | awk '/percentage:/  {print $2; exit}'
}

case $FIRST_ATTR in
  -i | --info )
    get_info ;;
  -b | --info-battery )
    get_info_batt ;;
esac
