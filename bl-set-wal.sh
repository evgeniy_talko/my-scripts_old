#!/usr/bin/env bash
  
WALL=$(cat ~/.cache/wal/wal)

betterlockscreen --update $WALL --blur 5.0;
