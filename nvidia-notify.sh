#!/bin/sh
# /usr/local/sbin/nvidia_event.sh

USER=my_username

case $1 in
	on)
		status="turned on"
		;;
	off)
		status="turned off"
		;;
esac

su $USER -c "export DISPLAY=:0.0; notify-send nvidia \"$status\"" || exit 0
