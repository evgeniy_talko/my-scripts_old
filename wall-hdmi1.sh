#!/usr/bin/env bash
  
WALL_FROM_NITROGEN=$(awk -F'=' '/file/ {print $2; exit}' "$HOME/.config/nitrogen/bg-saved.cfg")
feh --bg-fill "$WALL_FROM_NITROGEN"
