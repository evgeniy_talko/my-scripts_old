#!/usr/bin/env bash
  
PREV_WIN=$(bspc query -N -n prev.local.floating || bspc query -N -n any.local.floating)
bspc node --focus $PREV_WIN
