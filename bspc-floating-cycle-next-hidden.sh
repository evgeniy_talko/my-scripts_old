#!/usr/bin/env bash
  
NEXT_WIN=$(bspc query -N -n next.local.floating.hidden || bspc query -N -n any.local.floating.hidden)
bspc node focused.floating -g hidden=on
bspc node $NEXT_WIN -g hidden=off
bspc node $NEXT_WIN -f
