#!/usr/bin/env bash
  
if bspc query -N -n .hidden.local; then
    # If there are hidden windows show them and exit the script
    # this is to prevent having 2 sets of floating windows toggling each other
    for i in $(bspc query -N -n .hidden.local); do
        bspc node "$i" -g hidden=off
    done
    bspc node last.floating --focus
    exit 1
fi

for i in $(bspc query -N -n .floating.local); do
    # Otherwise, hide every floating window
    bspc node "$i" -g hidden=on
done
