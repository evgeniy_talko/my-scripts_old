#!/usr/bin/env bash
  
TODO=$(command -v todo.sh)
alacritty \
  --class dropdown_todo,todo.txt \
  --title "Tasks for today" \
  --command watch -c ${TODO} listpri A-B
