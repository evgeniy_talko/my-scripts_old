#!/bin/bash

ARG=$1
SOUND="/usr/share/sounds/deepin/stereo/audio-volume-change.wav"

DEF=$(eval "pacmd info | grep 'Default sink name'")
DEF=${DEF##*:}

if [ "$ARG" == "mute" ]; then
	eval "pactl set-sink-mute ${DEF} toggle"
else
  eval "pactl set-sink-volume ${DEF} ${ARG} && paplay $SOUND"
  # eval "pactl set-sink-volume ${DEF} ${ARG} && paplay /usr/share/sounds/sonar/stereo/audio-volume-change.* --audio-device=pulse/${DEF}"
fi

