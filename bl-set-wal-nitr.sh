#!/usr/bin/env bash
  
WALL=$(awk 'NR==2 {print $1}' ~/.config/nitrogen/bg-saved.cfg | sed 's/file=//g')

betterlockscreen --update $WALL --blur 5.0
