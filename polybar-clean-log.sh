PBRLOG_1="/tmp/polybar-mybar.log"
PBRLOG_2="/tmp/polybar-mybar-2.log"
PBRLOGLEN_1=$(cat < "$PBRLOG_1" | wc -l)
PBRLOGLEN_2=$(cat < "$PBRLOG_2" | wc -l)
[ "$PBRLOGLEN_1" -ge 60 ] && rm -rf "$PBRLOG_1"
[ "$PBRLOGLEN_2" -ge 60 ] && rm -rf "$PBRLOG_2"
