#!/usr/bin/bash
BACKUP_SOURCE="/home/evgeniy/03_Drafts"
BACKUP_DEVICE="/dev/sdd"
MOUNT_POINT="/mnt/external"


## Notify backup started:
notify-send "Backup from $BACKUP_SOURCE to $BACKUP_DEVICE started."

#check if mount point directory exists, if not create it
if [ ! -d "$MOUNT_POINT" ] ; then 
	/bin/mkdir  "$MOUNT_POINT"; 
fi

/bin/mount  -t  auto  "$BACKUP_DEVICE"  "$MOUNT_POINT"

#run a differential backup of files
/usr/bin/rsync -auz "$MOUNT_POINT" "$BACKUP_SOURCE" && /bin/umount "$BACKUP_DEVICE"

## Notify backup ended:
notify-send "Backup from $BACKUP_SOURCE done."
exit
