#!/usr/bin/env bash
  
mode="$(xrandr -q|grep -A1 "HDMI1 connected"| tail -1 |awk '{ print $1 }')"
if [ -n "$mode" ]; then
  xrandr --output eDP1 --off
  xrandr --output HDMI1 --primary --mode 3440x1440
fi
