#!/usr/bin/env bash
  
# set up the two monitors for bspwm
# NOTE This is a simplistic approach because I already know the settings I
# want to apply.
# XRANDR=$(which xrandr)
my_laptop_external_monitor=$(xrandr --query | grep 'HDMI-1')
if [[ $my_laptop_external_monitor = *connected* ]]; then
  xrandr --output eDP-1 --primary --mode 1920x1080 --rotate normal --output HDMI-1 --mode 3440x1440 --rotate normal --right-of eDP1
fi

