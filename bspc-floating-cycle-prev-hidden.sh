#!/usr/bin/env bash
  
PREV_WIN=$(bspc query -N -n prev.local.floating.hidden || bspc query -N -n any.local.floating.hidden)
bspc node focused.floating -g hidden=on
bspc node $PREV_WIN -g hidden=off
bspc node $PREV_WIN -f
