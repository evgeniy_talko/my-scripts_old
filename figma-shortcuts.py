#!/usr/bin/env python3
import pyautogui, sys, os
from pathlib import Path

ARGS = sys.argv
print(ARGS[1])

DIR = "figma-shortcuts/"
IMG = "figma-gamburger.png"

#  IMG_PATH = Path(os.getcwd(), DIR, IMG)
IMG_PATH = "/mnt/Data/Scripts/" + DIR + IMG

print(IMG_PATH)

COORDS = pyautogui.locateCenterOnScreen(str(IMG_PATH))
print(COORDS)

if COORDS:
    pyautogui.click(COORDS)
    pyautogui.write(ARGS[1])
    pyautogui.press("enter")
