#!/usr/bin/env bash

DEVICE_STYLUS=`xsetwacom list dev | grep -E -o ".*stylus"`
DEVICE_ERASER=`xsetwacom list dev | grep -E -o ".*eraser"`
DEVICE_CURSOR=`xsetwacom list dev | grep -E -o ".*cursor"`
DEVICE_PAD=`xsetwacom list dev | grep -E -o ".*pad"`
DEVICE_TOUCH=`xsetwacom list dev | grep -E -o ".*touch"` 

xsetwacom set "$DEVICE_PAD" "PanScrollThreshold" "-1000"
xsetwacom set "$DEVICE_TOUCH" "ZoomDistance" "120"
xsetwacom set "$DEVICE_TOUCH" "ScrollDistance" "80"
xsetwacom set "$DEVICE_TOUCH" Area 0 0 8180 6290
